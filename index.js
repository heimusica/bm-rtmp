var express = require('express'),
    app = express(),
    server = require('http').Server(app),
    io = require('socket.io')(server),
    exec = require('child_process').exec;

app.use(express.static('public'));
server.listen(80);

var currentStreams = { 'sdi0': null, 'sdi1': null },
    modes = { 2: { size: '720x576', framerate: 25 },
              12: { size: '1280x720', framerate: 59.9401 } };

function buildCmd(url, mode, port) {
  var cmd = 'ffmpeg -vsync 1 -i %url% -c:v rawvideo -pix_fmt uyvy422 -c:a pcm_s16le -s %size% -r %framerate% -ar 48000 -ac 2 -f nut -f_strict experimental -  |bmdplay -m %mode% -C %port% -O 4 - -f pipe:0';

  cmd = cmd.replace( '%url%', url )
           .replace( '%size%', modes[mode].size )
           .replace( '%framerate%', modes[mode].framerate )
           .replace( '%mode%', mode )
           .replace( '%port%', port.replace( 'sdi', '' ) );

  return cmd;
};


function startCapture( port, url, mode ) {
  currentStreams[ port ] = { port: port, url: url, mode: mode };

  var cmd = buildCmd( url, mode, port );

  currentStreams[ port ].proc = exec( cmd );

  currentStreams[ port ].proc.on( 'exit', function() {
    stopCapture( port );
  });

  setTimeout( function() {
    if( currentStreams[ port ] == null ) {
      stopCapture( port );
    } else {
      if( currentStreams[port].proc.exitCode == null ) {
        console.log( 'emitiendo captureon!' );
        io.emit( 'captureOn', { port: port } );
      } else {
        stopCapture( port );
      };
    };
  }, 3000 );
};

function stopCapture( port ) {
  console.log( 'stopCapture!', port, currentStreams, port );
  if( currentStreams[ port ] != null ) {
    var secondThread = currentStreams[ port ].proc.pid+1;
    exec( 'kill -9 ' + secondThread );
    currentStreams[ port ].proc.kill();
    currentStreams[ port ] = null;
  };
  io.emit( 'captureStopped', { port: port } );
};

io.on( 'connection', function( socket ) {
  socket.on( 'startCapture', function(e) {
    console.log( '-> startCapture', e );
    if( currentStreams[ e.port ] == null ) {
      console.log( '-> No hay streams en ' + e.port + '!' );
      startCapture( e.port, e.url, e.mode );
    };
  });

  socket.on( 'stopCapture', function(e) {
    console.log( '-> stopCapture', e );
    if( currentStreams[ e.port ] != null ) {
      stopCapture( e.port );
    };
  });
});
