$( document ).ready( function() {
  window.ws = io.connect( '/');

  $('.output').on( 'submit', function(e) {
    var port = $(this).data( 'port' ),
        state = $(this).data( 'state' ),
        url = $(this).find('.url').val(),
        mode = $(this).find('.mode').val();

    if( state == 'idle' ) {

      console.log( 'startCapture', port, url, mode );
      $(this).find( 'button' ).prop( 'disabled', true );
      $(this).find( 'button' ).text( 'Capturando...' );
      ws.emit( 'startCapture', { port: port, url: url, mode: mode } );
    };

    if( state == 'capture' ) {
      ws.emit( 'stopCapture', { port: port, url: url, mode: mode } );
    };

    e.preventDefault();

  });

  ws.on( 'captureStopped', function(e) {
    var btn = $('.output[data-port=' + e.port + '] button')
        btn.text( 'Capturar' );
        btn.prop('disabled', false );
    $('.output[data-port=' + e.port + ']').data('state', 'idle');
    
  });

  ws.on( 'captureOn', function(e) {
    var btn = $('.output[data-port=' + e.port + '] button')
        btn.text( 'Detener' );
        btn.prop('disabled', false );
    $('.output[data-port=' + e.port + ']').data('state', 'capture');

  });
});
